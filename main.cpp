#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */
#include <errno.h>
#include <cstring>

#include <libnetfilter_queue/libnetfilter_queue.h>

#define LIBNET_LIL_ENDIAN 1
typedef struct libnet_ipv4_hdr
{
#if (LIBNET_LIL_ENDIAN)
    u_int8_t ip_hl:4,      /* header length */
           ip_v:4;         /* version */
#endif
#if (LIBNET_BIG_ENDIAN)
    u_int8_t ip_v:4,       /* version */
           ip_hl:4;        /* header length */
#endif
    u_int8_t ip_tos;       /* type of service */
#ifndef IPTOS_LOWDELAY
#define IPTOS_LOWDELAY      0x10
#endif
#ifndef IPTOS_THROUGHPUT
#define IPTOS_THROUGHPUT    0x08
#endif
#ifndef IPTOS_RELIABILITY
#define IPTOS_RELIABILITY   0x04
#endif
#ifndef IPTOS_LOWCOST
#define IPTOS_LOWCOST       0x02
#endif
    u_int16_t ip_len;         /* total length */
    u_int16_t ip_id;          /* identification */
    u_int16_t ip_off;
#ifndef IP_RF
#define IP_RF 0x8000        /* reserved fragment flag */
#endif
#ifndef IP_DF
#define IP_DF 0x4000        /* dont fragment flag */
#endif
#ifndef IP_MF
#define IP_MF 0x2000        /* more fragments flag */
#endif 
#ifndef IP_OFFMASK
#define IP_OFFMASK 0x1fff   /* mask for fragmenting bits */
#endif
    u_int8_t ip_ttl;          /* time to live */
    u_int8_t ip_p;            /* protocol */
    u_int16_t ip_sum;         /* checksum */
    struct in_addr ip_src, ip_dst; /* source and dest address */
}IP_hdr;

/*
 *  TCP header
 *  Transmission Control Protocol
 *  Static header size: 20 bytes
 */
typedef struct libnet_tcp_hdr
{
    u_int16_t th_sport;       /* source port */
    u_int16_t th_dport;       /* destination port */
    u_int32_t th_seq;          /* sequence number */
    u_int32_t th_ack;          /* acknowledgement number */
#if (LIBNET_LIL_ENDIAN)
    u_int8_t th_x2:4,         /* (unused) */
           th_off:4;        /* data offset */
#endif
#if (LIBNET_BIG_ENDIAN)
    u_int8_t th_off:4,        /* data offset */
           th_x2:4;         /* (unused) */
#endif
    u_int8_t  th_flags;       /* control flags */
#ifndef TH_FIN
#define TH_FIN    0x01      /* finished send data */
#endif
#ifndef TH_SYN
#define TH_SYN    0x02      /* synchronize sequence numbers */
#endif
#ifndef TH_RST
#define TH_RST    0x04      /* reset the connection */
#endif
#ifndef TH_PUSH
#define TH_PUSH   0x08      /* push data to the app layer */
#endif
#ifndef TH_ACK
#define TH_ACK    0x10      /* acknowledge */
#endif
#ifndef TH_URG
#define TH_URG    0x20      /* urgent! */
#endif
#ifndef TH_ECE
#define TH_ECE    0x40
#endif
#ifndef TH_CWR   
#define TH_CWR    0x80
#endif
    u_int16_t th_win;         /* window */
    u_int16_t th_sum;         /* checksum */
    u_int16_t th_urp;         /* urgent pointer */
}TCP_hdr;

#define MAX_STR_SIZE 1557
char* blacklist;
int blacklist_len;

void dump(unsigned char* buf, int size) {
	int i;
	for (i = 0; i < size; i++) {
		if (i != 0 && i % 16 == 0)
			printf("\n");
		printf("%02X ", buf[i]);
	}
	printf("\n");
}


/* returns packet id */
static u_int32_t print_pkt (struct nfq_data *tb)
{
	int id = 0;
	struct nfqnl_msg_packet_hdr *ph;
	struct nfqnl_msg_packet_hw *hwph;
	u_int32_t mark,ifi;
	int payload_len;
	unsigned char *data;

	ph = nfq_get_msg_packet_hdr(tb);
	if (ph) {
		id = ntohl(ph->packet_id);
	}

	payload_len = nfq_get_payload(tb, &data);
	if (payload_len >= 0)
		printf("payload_len=%d\n", payload_len);

	int hdr_size;

	if(((IP_hdr*)data) -> ip_v == 4) //ipv4
	{
		//printf("IPv4\n");
		if(((IP_hdr*)data) -> ip_p != 6)
		{
			//printf("not TCP pkt\n");
		}
		else
		{
			hdr_size = ((IP_hdr*)data) -> ip_hl * 4;
			if(ntohs(((TCP_hdr*)((uint8_t*)data + hdr_size)) -> th_dport) != 80)
			{
				//printf("not port 80\n");
			}
			else
			{
				hdr_size += ((TCP_hdr*)((uint8_t*)data + hdr_size)) -> th_off * 4;
				
				//dump((uint8_t*)data + hdr_size, payload_len - hdr_size);
				if(payload_len - hdr_size < 3)
				{
					printf("not enough payload len\n");
					printf("hdr_size : %d\n", hdr_size);
				}
				else if(memcmp((uint8_t*)data + hdr_size, "GET", 3) == 0 || memcmp((uint8_t*)data + hdr_size, "POST", 4))
				{	//hello http response
					int host_offset = 0;
					for(int i = 0; i < payload_len - hdr_size; i++)
					{
						if(*((char*)data + hdr_size + i) == '\n')
						{
							host_offset = i + 1;
							break;
						}
					}
					if(host_offset == 0)
					{
						printf("cannot find LF in packet\n");
					}	
					else
					{
						int host_end_offset = 0;
						for(int i = host_offset; i < payload_len - hdr_size - host_offset; i++)
						{
							if(*((char*)data + hdr_size + i) == '\n')
							{
								host_end_offset = i + 1; 
								break;
							}
						}
						if(host_end_offset == 0)
						{
							printf("cannot find LF in packet\n");
						}
						else
						{
							dump((uint8_t*)data + hdr_size + host_offset, host_end_offset - host_offset);
							if(host_end_offset - host_offset - 6 >= blacklist_len)
							{
								if(memcmp((uint8_t*)data + hdr_size + host_offset + 6, blacklist, blacklist_len) == 0)
								{
									printf("ban this\n");
									id = 0;
								}
							}
						}
					}
				}	
				 
				fputc('\n', stdout);
			}
		}
	}
	else if(((IP_hdr*)data) -> ip_v == 6) //ipv6
	{
		printf("IPv6 - not prviding this\n");
		/*
		hdr_size == sizeof(IPv6_hdr);
		while(((IPv6_hdr*)((uint8_t*)data + hdr_size)) -> next_header != 6)
		{
			printf("rec\n");
			hdr_size += 8;
			if(hdr_size > payload_len)
			{
				printf("cannot parse IPv6 header to tcp header\n");
				return 0;
			}
		}*/
	}
	return id;
}


static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
	      struct nfq_data *nfa, void *data)
{
	uint32_t id = print_pkt(nfa);
	int ban = 0;
	//printf("entering callback\n");
	if(id != 0)
	{
		return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
	}
	else
	{
		return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
	}
}

int main(int argc, char **argv)
{
	struct nfq_handle *h;
	struct nfq_q_handle *qh;
	struct nfnl_handle *nh;
	int fd;
	int rv;
	char buf[4096] __attribute__ ((aligned));

	blacklist = (char*)malloc(sizeof(char) * MAX_STR_SIZE);
	for(int i = 0; ; i++)
	{
		*(blacklist + i) = argv[1][i];
		if(argv[1][i] == '\0')
		{
			blacklist_len = i;
			break;
		}
	}

	printf("opening library handle\n");
	h = nfq_open();
	if (!h) {
		fprintf(stderr, "error during nfq_open()\n");
		exit(1);
	}

	printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
	if (nfq_unbind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_unbind_pf()\n");
		exit(1);
	}

	printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
	if (nfq_bind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_bind_pf()\n");
		exit(1);
	}

	printf("binding this socket to queue '0'\n");
	qh = nfq_create_queue(h,  0, &cb, NULL);
	if (!qh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}

	printf("setting copy_packet mode\n");
	if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}

	fd = nfq_fd(h);

	for (;;) {
		if ((rv = recv(fd, buf, sizeof(buf), 0)) >= 0) {
			//printf("pkt received\n");
			nfq_handle_packet(h, buf, rv);
			continue;
		}
		/* if your application is too slow to digest the packets that
		 * are sent from kernel-space, the socket buffer that we use
		 * to enqueue packets may fill up returning ENOBUFS. Depending
		 * on your application, this error may be ignored. nfq_nlmsg_verdict_putPlease, see
		 * the doxygen documentation of this library on how to improve
		 * this situation.
		 */
		if (rv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}

	printf("unbinding from queue 0\n");
	nfq_destroy_queue(qh);

#ifdef INSANE
	/* normally, applications SHOULD NOT issue this command, since
	 * it detaches other programs/sockets from AF_INET, too ! */
	printf("unbinding from AF_INET\n");
	nfq_unbind_pf(h, AF_INET);
#endif

	printf("closing library handle\n");
	nfq_close(h);
	free(blacklist);
	exit(0);
}
